Creating a symbolic within the backend directory to the dist of the frontend.
This allows the backend to serve the static frontend files.

```zsh
ln -s ../frontend/dist ./dist
```

Add yew dependency to the backend's cargo.toml file with features "ssr"
tokio too?

Add the frontend crate to the backend cargo.toml
```toml
frontend = { path = "../frontend" }
```

In the backend src/main.rs, see details the render_yew_app function as
this function allows for the server side rendering of yew. 
