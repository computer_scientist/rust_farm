use actix_files::{self, NamedFile};

use actix_web::middleware::Logger;
use actix_web::{get, App, HttpRequest, HttpServer};
use env_logger::Env;
use openssl::ssl::{SslAcceptor, SslFiletype, SslMethod};
use std::io::ErrorKind;

#[get("/")]
async fn index(_req: HttpRequest) -> Result<NamedFile, std::io::Error> {
    actix_files::NamedFile::open("./dist/index.html").map_err(|_| {
        eprintln!("Failed to open index file");
        std::io::Error::new(ErrorKind::Other, "Failed to open index file")
    })
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::init_from_env(Env::default().default_filter_or("info"));

    let mut builder = match SslAcceptor::mozilla_intermediate(SslMethod::tls()) {
        Ok(builder) => builder,
        Err(e) => {
            eprintln!("Failed to create builder: {}", e);
            std::process::exit(1);
        }
    };

    match builder.set_private_key_file("key.pem", SslFiletype::PEM) {
        Ok(_) => {
            println!("Successfully set openssl key!")
        }
        Err(e) => {
            eprintln!("Failed to set openssl key: {}", e);
            std::process::exit(1);
        }
    };

    match builder.set_certificate_chain_file("cert.pem") {
        Ok(_) => {
            println!("Successfully set openssl certificate!");
        }
        Err(e) => {
            eprintln!("Failed to set openssl certificate: {}", e);
            std::process::exit(1);
        }
    };

    println!("Listening on https://127.0.0.1:4443...");

    HttpServer::new(|| App::new().wrap(Logger::default()).service(index))
        .bind_openssl("127.0.0.1:4443", builder)?
        .run()
        .await
}
